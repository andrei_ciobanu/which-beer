package com.whichbeer;

import android.app.Activity;
import android.os.Bundle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.whichbeer.model.BeerResult;
import com.whichbeer.model.LocationResult;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrei on 4/5/2014.
 */
public class LocationsActivity extends Activity {

    static final LatLng HAMBURG = new LatLng(53.558, 9.927);
    static final LatLng KIEL = new LatLng(53.551, 9.993);

    public void onCreate(Bundle savedInstanceState) {
        /*super.onCreate(savedInstanceState);
        setContentView(R.layout.location);

        GoogleMap map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        List<MarkerOptions> markerOptionses = parseResponse();
        for (MarkerOptions markerOption : markerOptionses){
            map.addMarker(markerOption);
        }

        if (!markerOptionses.isEmpty()) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(markerOptionses.iterator().next().getPosition(),15));
        }

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                return true;
            }
        });

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Toast.makeText(getBaseContext(),marker.getTitle(),
                        Toast.LENGTH_SHORT).show();
            }
        });*/

    }


    private List<MarkerOptions> parseResponse(){
        List<MarkerOptions> markers = new ArrayList<MarkerOptions>();

        InputStreamReader data = null;
        try {
            Gson gson = new Gson();
            Type collectionType = new TypeToken<List<LocationResult>>(){}.getType();

            data = new InputStreamReader(getApplicationContext().getAssets().open("data.json"));
            List<LocationResult> result = gson.fromJson(data, collectionType);

            for (LocationResult location : result) {
                if(location.hasGeocode()) {
                    MarkerOptions markerOptions = new MarkerOptions()
                            .position(location.getGeo())
                            .title(location.getLocationName())
                            .snippet(getProbability(location.getBeers()));
                    markers.add(markerOptions);
                }
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            close(data);
        }

        return markers;

    }

    private String getProbability(List<BeerResult> beers) {
        float probability = 0;
        for(BeerResult beerResult : beers){
            probability += beerResult.getProbability();
        }

        if(!beers.isEmpty()){
            return "" + probability / beers.size() + "%";
        }

        return "0%";
    }

    private void close(Closeable closeable) {
        if(closeable != null){
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}