package com.whichbeer;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Window;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.whichbeer.model.BeerResult;
import com.whichbeer.model.LocationResult;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrei on 4/6/2014.
 */
public class MapActivity extends Activity {

    private GoogleMap map;
    private Location lastKnownLocation;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        lastKnownLocation = (Location) intent.getExtras().get(LocationFinder.LAST_KNOWN_LOCATION);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.results_map);
        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (map == null) {
            // Try to obtain the map from the SupportMapFragment.
            map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (map != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        List<MarkerOptions> markerOptions = parseResponse();

        for (MarkerOptions markerOption : markerOptions){
            map.addMarker(markerOption);
        }

        if(lastKnownLocation!=null) {
            LatLng lastLatLng = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
            map.addMarker(new MarkerOptions().position(lastLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(lastLatLng, 15));
        } else if (!markerOptions.isEmpty()) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(markerOptions.iterator().next().getPosition(), 15));
        }

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                return true;
            }
        });


        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
            }
        });
    }


    private List<MarkerOptions> parseResponse(){
        List<MarkerOptions> markers = new ArrayList<MarkerOptions>();

        InputStreamReader data = null;
        try {
            Gson gson = new Gson();
            Type collectionType = new TypeToken<List<LocationResult>>(){}.getType();

            data = new InputStreamReader(getApplicationContext().getAssets().open("data.json"));
            List<LocationResult> result = gson.fromJson(data, collectionType);

            for (LocationResult location : result) {
                if(location.hasGeocode()) {
                    MarkerOptions markerOptions = new MarkerOptions()
                            .position(location.getGeo())
                            .title(location.getLocationName())
                            .snippet(location.getAddress())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.beer_marker));
                    markers.add(markerOptions);
                }
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            close(data);
        }

        return markers;

    }

    private String getProbability(List<BeerResult> beers) {
        float probability = 0;
        for(BeerResult beerResult : beers){
            probability += beerResult.getProbability();
        }

        if(!beers.isEmpty()){
            return "" + probability / beers.size() + "%";
        }

        return "0%";
    }

    private void close(Closeable closeable) {
        if(closeable != null){
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}