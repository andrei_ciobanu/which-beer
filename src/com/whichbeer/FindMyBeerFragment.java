package com.whichbeer;

import android.app.Fragment;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.whichbeer.model.BeerItem;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.whichbeer.LocationFinder.LAST_KNOWN_LOCATION;

/**
 * Created by Andrei on 4/5/2014.
 */
public class FindMyBeerFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final String BEER_NAME = "beerName";
    private static final String[] FROM = {BEER_NAME};
    private static final int[] TO = {R.id.beerName};

    private ListView listView;
    private SearchActivity searchActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.find_my_beer, container,false);
        listView = (ListView) v.findViewById(R.id.listView);
        listView.setOnItemClickListener(this);
        listView.setAdapter(new SimpleAdapter(listView.getContext(), getData(), R.layout.beer_item, FROM, TO));
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        searchActivity = (SearchActivity) getActivity();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Location lastKnownLocation = searchActivity.getLastKnownLocation();

        Intent intent = new Intent(view.getContext(), MapActivity.class);
        intent.putExtra(LAST_KNOWN_LOCATION, lastKnownLocation);
        startActivity(intent);
    }

    private List<Map<String, Object>> getData(){

        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        InputStreamReader data = null;
        try {
            Gson gson = new Gson();
            Type collectionType = new TypeToken<List<BeerItem>>(){}.getType();

            data = new InputStreamReader(getActivity().getApplicationContext().getAssets().open("beers.json"));
            List<BeerItem> items = gson.fromJson(data, collectionType);

            for (BeerItem item : items) {
                Map<String, Object> itemMap = new HashMap<String, Object>();
                itemMap.put(BEER_NAME, item.getName());
                list.add(itemMap);
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            close(data);
        }

        return list;

    }
    private void close(Closeable closeable) {
        if(closeable != null){
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}