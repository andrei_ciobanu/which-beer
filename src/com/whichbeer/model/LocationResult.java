package com.whichbeer.model;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrei on 4/5/2014.
 */
public class LocationResult {
    String locationName;
    String address;
    List<BeerResult> beers = new ArrayList<BeerResult>();
    float geo[];

    public String getLocationName() {
        return locationName;
    }

    public LatLng getGeo() {
        if(hasGeocode()) {
            return new LatLng(geo[0], geo[1]);
        }
        return null;
    }

    public boolean hasGeocode() {
        return geo != null && geo.length == 2;
    }


    public List<BeerResult> getBeers() {
        return beers;
    }

    public String getAddress() {
        return address;
    }
}
