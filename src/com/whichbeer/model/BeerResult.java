package com.whichbeer.model;

/**
 * Created by Andrei on 4/5/2014.
 */
public class BeerResult {
    long id;
    float probability;

    public float getProbability() {
        return probability;
    }
}
