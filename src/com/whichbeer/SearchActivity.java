package com.whichbeer;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import static com.whichbeer.LocationFinder.LAST_KNOWN_LOCATION;

/**
 * Created by Andrei on 4/5/2014.
 */
public class SearchActivity extends Activity implements View.OnClickListener {

    private Location lastLocation = null;
    private LocationFinder locationFinder;
    private Location lastKnownLocation;

    private Button findMyBeerButton;
    private Button discoverNewBeerButton;
    private Button getMeABeer;


    private View findMyBeerFragment;
    private View discoverBeerFragment;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.search);
        locationFinder = new LocationFinder(getApplicationContext());
        locationFinder.start();

        findMyBeerButton = (Button) findViewById(R.id.findBeerButton);
        discoverNewBeerButton = (Button) findViewById(R.id.discoverBeerButton);

        getMeABeer = (Button) findViewById(R.id.getMeABeerButton);

        findMyBeerFragment = findViewById(R.id.findBeerFragment);
        discoverBeerFragment = findViewById(R.id.discoverBeerFragment);

        findBeer();

        findMyBeerButton.setOnClickListener(this);
        discoverNewBeerButton.setOnClickListener(this);
        getMeABeer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.findBeerButton:
                findBeer();
                break;
            case R.id.discoverBeerButton:
                discoverBeer();
                break;
            case R.id.getMeABeerButton:
                Location lastKnownLocation = getLastKnownLocation();
                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                intent.putExtra(LAST_KNOWN_LOCATION, lastKnownLocation);
                startActivity(intent);
                break;
            default:
        }
    }

    private void discoverBeer() {
        findMyBeerFragment.setVisibility(View.GONE);
        discoverBeerFragment.setVisibility(View.VISIBLE);
    }

    private void findBeer() {
        findMyBeerFragment.setVisibility(View.VISIBLE);
        discoverBeerFragment.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationFinder.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationFinder.stop();
    }

    public Location getLastKnownLocation() {
        return locationFinder.getLastKnownLocation();
    }
}